package fr.rcdsm.nicolasklein.optimisation_tp1;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by orion on 19/03/15.
 */
public class Terrain extends View {

    private ArrayList<Balle> lesBalles;

    public Terrain(Context context) {
        super(context);
        init();
    }

    public Terrain(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Terrain(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        lesBalles = new ArrayList<Balle>();
    }

    public void genereBalles(int nb, int height, int width, Balle.TypeBalle type){
        for (int i = 0; i<nb; i++){
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);
            lesBalles.add(new Balle(x, y, type));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for(int i=0; i<lesBalles.size();i++){
            lesBalles.get(i).draw(canvas);
        }
    }

    public void mouvementDeToutesLesBalles(){
        for(int i=0; i<lesBalles.size();i++){
            lesBalles.get(i).mouvement(this);
        }
    }

    public void traiteLesCollisions(){
        for(int i=0; i<lesBalles.size();i++){
            Balle b = whoIsCollisioningWith(lesBalles.get(i));
            if (b!=null) lesBalles.get(i).traiteCollision(b);
        }
    }

    public Balle whoIsCollisioningWith(Balle b){
        for(int i=0; i<lesBalles.size();i++){
            if(b != lesBalles.get(i)){
                if(lesBalles.get(i).isCollisioning(b)) return lesBalles.get(i);
            }
        }
        return null;
    }
}
