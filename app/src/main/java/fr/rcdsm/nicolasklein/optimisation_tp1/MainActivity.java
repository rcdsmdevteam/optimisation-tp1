package fr.rcdsm.nicolasklein.optimisation_tp1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {

    Timer timer;
    TimerTask timerTask;
    Terrain t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Integer h = getResources().getDisplayMetrics().heightPixels;
        Integer w = getResources().getDisplayMetrics().widthPixels;

        t = (Terrain)findViewById(R.id.view);

        t.genereBalles(50, h, w, Balle.TypeBalle.Bleu );
        t.genereBalles(100, h, w, Balle.TypeBalle.Rouge);
        t.genereBalles(50, h, w, Balle.TypeBalle.Vert);
        t.genereBalles(20, h, w, Balle.TypeBalle.Noir);

        starTimer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void starTimer(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                while(true){
                    t.mouvementDeToutesLesBalles();
                    t.traiteLesCollisions();
                }
            }
        };

        Thread th = new Thread(r);
        th.start();

        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        t.invalidate();
                    }
                });
            }
        };

        timer.schedule(timerTask, 500, 10);
    }
}
