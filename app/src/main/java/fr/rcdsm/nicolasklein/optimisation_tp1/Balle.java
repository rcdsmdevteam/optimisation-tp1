package fr.rcdsm.nicolasklein.optimisation_tp1;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by orion on 19/03/15.
 */
public class Balle {
    private static Paint paintRouge = null;
    private static Paint paintBleu = null;
    private static Paint paintVert = null;
    private static Paint paintNoir = null;
    private Paint paint;
    private int x, y;
    private float vx, vy, rayon;
    private TypeBalle type;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getVx() {
        return vx;
    }
    public void setVx(float vx) {
        this.vx = vx;
    }

    public float getVy() {
        return vy;
    }
    public void setVy(float vy) {
        this.vy = vy;
    }

    public float getRayon() {
        return rayon;
    }
    public void setRayon(float rayon) {
        this.rayon = rayon;
    }

    public TypeBalle getType() {
        return type;
    }
    public void setType(TypeBalle type) {
        this.type = type;
    }


    public Balle(int x, int y, TypeBalle type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.rayon = 20f;
        this.vx = ((float)Math.random() * 10f - 5f)*0.3f;
        this.vy = ((float)Math.random() * 10f - 5f)*0.3f;

        if( paintBleu == null ){
            paintBleu = new Paint();
            paintBleu.setColor(Color.BLUE);
        }
        if( paintVert == null ){
            paintVert = new Paint();
            paintVert.setColor(Color.GREEN);
        }
        if( paintNoir == null ){
            paintNoir = new Paint();
            paintNoir.setColor(Color.BLACK);
        }
        if( paintRouge == null ){
            paintRouge = new Paint();
            paintRouge.setColor(Color.RED);
        }

        switch (this.getType()){
            case Bleu:
                paint = paintBleu;
                break;
            case Rouge:
                paint = paintRouge;
                break;
            case Vert:
                paint = paintVert;
                break;
            case Noir:
                paint = paintNoir;
                break;
        }
    }

    public enum TypeBalle{
        Bleu,
        Rouge,
        Vert,
        Noir
    }


    void draw(Canvas cv){
        cv.drawCircle(this.getX(), this.getY(), this.getRayon(), paint);
    }

    public void mouvement(Terrain terrain){
        if(getX() - getRayon() < 0){
            //setX((int)getRayon()*2);
            setVx(-getVx());
        }
        if(getX() + getRayon() > terrain.getWidth()){
            //setX(terrain.getWidth() - (int)getRayon()*2);
            setVx(-getVx());
        }

        if(getY() - getRayon() < 0){
            //setY((int)getRayon()*2);
            setVy(-getVy());
        }
        if(getY() + getRayon() > terrain.getHeight()){
            //setY(terrain.getHeight() - (int)getRayon()*2);
            setVy(-getVy());
        }

        setX((int)(getX() + getVx()));
        setY((int)(getY() + getVy()));
    }

    boolean isCollisioning(Balle b){
        float dx = b.getX() - this.getX();
        float dy = b.getY() - this.getY();
        float d = (float)Math.sqrt(dx*dx + dy*dy);
        return d < this.getRayon() + b.getRayon();

    }

    public void traiteCollision(Balle b){
        setVx(-getVx());
        setVy(-getVy());
        if(getX() > b.getX() - b.getRayon() && getX() < b.getX() + b.getRayon()){
            setVx(-getVx());
        }
        if(getY() > b.getY() - b.getRayon() && getY() < b.getY() + b.getRayon()){
            setVy(-getVy());
        }
    }
}